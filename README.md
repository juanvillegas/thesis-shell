# tesis-cli

Cliente ejemplo para conectarse con el servidor del Framework para el proyecto de tesis.
La UI esta preparada para moviles. Si ejecuta la aplicación en una pantalla de mayor tamaño, puede utilizar
*/static/phone.html* para acceder a una versión mobile simulada.

## Setup, Build & Run

Instrucciones:

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

>Creado por **Juan Manuel Villegas** @ 2017
