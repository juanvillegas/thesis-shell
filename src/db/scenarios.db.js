export default [
	{
		"id": "flybot",
		"name": "Reserva de Pasajes",
		"description": "Consulte acerca del estado de su vuelo y adquiera pasajes conversando con un agente de ventas de la aerolínea en el aeropuerto.",
		"extended_description": "Este escenario lo situa en un aeropuerto. La ventanilla de atención al publico esta colmada de gente, por lo que " +
								"se decide a utilizar la terminal de autoayuda, atendida por un agente virtual.",
		"images": {
			"portrait": {
				"path": "scenarios/fr-portrait.jpg",
				"alt": ""
			}
		},
		"objectives": [
			[
				"Consulte el estado de su vuelo. Su código de vuelo es AA-476",
				"Reserve un nuevo vuelo partiendo desde la ciudad de Viedma o Bahía Blanca, con destino Buenos Aires.",
                "La fecha de partida puede ser cualquier día de Noviembre",
                "No tienes equipaje para despachar."
			],
			[
				"Reserve un vuelo desde la ciudad de Buenos Aires hacia Posadas.",
                "Es urgente! Debes conseguir un vuelo para mañana a la noche.",
                "Tienes equipaje para despachar."
			]
		],
		"characters": [
			{
				"name": "Frank Stevens",
				"occupation": "Estudiante",
				"type": "player",
				"images": {
					"portrait": {
						"path": "scenarios/fr-user1.png",
						"alt": ""
					}
				}
			},
			{
				"name": "Amanda Clarke",
				"occupation": "Asistente de ventas en Aeropuerto de Ezeiza",
				"type": "system",
				"images": {
					"portrait": {
						"path": "scenarios/fr-cpu1.png",
						"alt": ""
					}
				}
			}
		]
	},

	{
	    "disabled": true,
		"id": "weabot",
		"name": "Experto en Clima",
		"description": "Charle con un experto en clima acerca de posibles eventos meteorológicos futuros!",
		"extended_description": "Extended",
		"images": {
			"portrait": {
				"path": "scenarios/wr-portrait.jpg",
				"alt": ""
			}
		},
		"characters": [
			{
				"name": "Elise",
				"occupation": "Gerente de ventas de HVO",
				"type": "player",
				"images": {
					"portrait": {
						"path": "scenarios/fr-cpu1.png",
						"alt": ""
					}
				}
			},
			{
				"name": "Jhon",
				"occupation": "Experto en Clima",
				"type": "system",
				"images": {
					"portrait": {
						"path": "scenarios/fr-cpu1.png",
						"alt": ""
					}
				}
			}
		]
	}
];