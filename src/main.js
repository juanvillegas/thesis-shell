import Vue from 'vue'
import App from './App'
import User from './services/User'
import Game from './services/Game'
import lodash from 'lodash';
import jQuery from 'jquery';
import Popper from 'popper.js';
import Tooltip from 'tooltip.js';

window.$ = jQuery;
window._ = lodash;
window.Tooltip = Tooltip;

window.Thesis = window.Thesis || {};

window.Thesis.Config = window.Thesis.Config || {};
window.Thesis.Config.botRemote = process.env.BOT_SERVER;

window.Thesis.Game = new Game();
window.Thesis.User = new User();
Thesis.User.init();

import router from './router'

Vue.config.productionTip = false;

window.MasterVue = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});


