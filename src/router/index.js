import Vue from 'vue'
import Router from 'vue-router'

import 'vue-toast/dist/vue-toast.min.css'

import Start from '@/components/Start'
import ScenarioSelector from '@/components/ScenarioSelector'
import ScenarioLauncher from '@/components/ScenarioLauncher'
import GameArena from '@/components/GameArena'
import Logout from '@/components/Logout'

Vue.use(Router);

const router = new Router({
	routes: [
		{
			path: '/start',
			name: 'Start',
			component: Start
		}, {
			path: '/scenarios',
			name: 'ScenarioSelector',
			component: ScenarioSelector,
			props: true
		}, {
			path: '/launcher',
			name: 'ScenarioLauncher',
			component: ScenarioLauncher,
			props: true
		}, {
			path: '/arena',
			name: 'Arena',
			component: GameArena,
			props: true
		}, {
			path: '/logout',
			name: 'Logout',
			component: Logout
		}
	]
});

router.beforeEach((to, from, next) => {
	if (to.path == '/') {
		next({ name: 'Start' });
		return;
	}

	if (to.name != 'Start') {
		// needs to be logged in
		if (!window.Thesis.User.check()) {
			next({name: 'Start' });
			return;
		}
	} else {
		if (window.Thesis.User.check()) {
			next({name: 'ScenarioSelector' });
			return;
		}
	}
	next();
});


export default router;