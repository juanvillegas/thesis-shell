export default class EventSubscriber {

	constructor() {
		this.subscribers = {};
	}

	fireSubscriptions(eventName) {
		if (this.subscribers[eventName]) {
			this.subscribers[eventName].forEach(fn => {
				fn();
			});
		}
	}

	subscribe(name, fn) {
		if (! this.subscribers[name]) {
			throw 'The provided event name is not supported by this object';
		}
		this.subscribers[name].push(fn);
	}

}