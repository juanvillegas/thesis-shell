import EventSubscriber from './EventSubscriber';

export default class Game extends EventSubscriber {

	constructor() {
		super();
		this._gamePoints = 0;
		this._goals = [];
		this._instructions = [];
		this._status = false;

		this.subscribers.statusChanged = [];
		this.subscribers.gameStarted = [];
		this.subscribers.gameEnded = [];
		this.subscribers.pointsChanged = [];
		this.subscribers.gameGoalsChanged = [];
	}

	set status(val) {
		this._status = val;
	}

	get status() {
		return this._status;
	}

	startGame(data) {
		this._status = true;
		this.fireSubscriptions('statusChanged');
		this.fireSubscriptions('gameStarted');
	}

	endGame() {
		this._status = false;
		this.fireSubscriptions('statusChanged');
		this.fireSubscriptions('gameEnded');
	}

	setInstructions(instructions) {
	    this._instructions = instructions;
    }

	setGoals(goals) {
		this._goals = goals;
		this.fireSubscriptions('gameGoalsChanged');
	}

	get gamePoints() {
		return this._gamePoints;
	}

	get goals() {
		return this._goals;
	}

	get instructions() {
		return this._instructions;
	}

}