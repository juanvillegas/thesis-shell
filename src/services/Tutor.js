export default class Tutor {


    constructor() {
        this._dictionary = {};
    }

	/**
	 *
	 * @param {array} errors
	 * @param {string} message
	 *
	 * @return {string}
	 */
	applySpellErrors(errors, message) {
		let formatted = '';
		let current_index = 0;

		errors.forEach(spell_error => {
			formatted += message.substr(current_index, spell_error.offset - current_index);

			formatted += '<span class="spell-error" data-title="' + this.getErrorTip(spell_error) + '">';
			formatted += spell_error.bad;
			formatted += '</span>';

			current_index = spell_error.offset+ spell_error.length;
		});

		if (current_index < message.length) {
			formatted += message.substr(current_index);
		}

		return formatted;
	}

    applyDictionaryMarkup(message, dictionary) {
	    let formatted_message = message;

	    _.each(dictionary, d => {
	        if (! d.discarded) {
                formatted_message = formatted_message.replace(new RegExp('' + d.original, 'ig'), '<span class="dict-entry" data-index="' + d.index + '">' + d.original + '</span>');
            }
        });

	    return formatted_message;
    }

	getErrorTip(spell_error) {
		let suggestions = spell_error.better;

		if (suggestions.length > 5) {
			suggestions = suggestions.slice(0, 5);
		}

		return suggestions.join(', ');
	}

	bakeSpellErrors(errors) {
		return 'Ups! Aparentemente la frase ingresada contiene ' + errors.length + ' o más errores. Revisa las sugerencias en tu mensaje y vuelve a enviarlo.';
	}

	closeDictionary() {
        this._dictionary.show = false;
    }

    setDictionaryActiveItem(item) {
        this._dictionary.active_definition = item;
        this._dictionary.show = true;
    }

}