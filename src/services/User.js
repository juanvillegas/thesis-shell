import EventSubscriber from './EventSubscriber';

export default class User extends EventSubscriber {

	constructor() {
		super();
		
		this.subscribers.authChanged = [];
		
		this._username = '';
		this._points = 0;
	}

	init() {
		let user = localStorage.getItem('thesis.User');
		if (user) {
			user = JSON.parse(user);
			this._username = user.username;
			this._points = user.points;
		}

		this.fireSubscriptions('authChanged');
	}

	check() {
		let user = localStorage.getItem('thesis.User');
		return !!user;
	}

	persist() {
		localStorage.setItem('thesis.User', JSON.stringify({
			username: this._username,
			points: this._points
		}));
	}

	clearPersist() {
		localStorage.removeItem('thesis.User');
	}

	login(userObject) {
		this._username = userObject.username;
		this._points = userObject.points || 0;

		this.persist();

		this.fireSubscriptions('authChanged');
	}

	logout() {
		this._username = '';
		this._points = 0;

		this.clearPersist();

		this.fireSubscriptions('authLogout');
		this.fireSubscriptions('authChanged');
	}

	get points() {
		return this._points;
	}

	set points(p) {
		this._points = p;
	}

	get username() {
		return this._username;
	}

	set username(u) {
		this._username = u;
	}

}